import unittest
import time
from north_utils.test import assert_equal, assert_not_equal, assert_float_equal, assert_vector_equal
from north_c9.controller import C9Controller
from north_robots.spin_coater import SpinCoater


class TestSpinCoater(unittest.TestCase):
    controller: C9Controller = None

    @classmethod
    def setUpClass(cls):
        cls.controller = C9Controller(address=2)

    @classmethod
    def tearDownClass(cls):
        cls.controller.disconnect()

    def setUp(self):
        time.sleep(1)
        self.spin_coater = SpinCoater(self.controller)

    def test_home(self):
        self.spin_coater.home()
        assert_float_equal(self.spin_coater.axis.position, 0, 10)
        assert_float_equal(self.spin_coater.axis.position_degrees, 0, 10)

    def test_basic_spin(self):
        self.spin_coater.spin(1000, 5000)
        time.sleep(1)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 1000, 100)
        self.spin_coater.spin_stop()
        time.sleep(1)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 0)

    def test_spin_with_duration(self):
        start_time = time.time()
        self.spin_coater.spin(1000, 5000, 1)
        time.sleep(0.5)
        assert_float_equal(time.time() - start_time, 1, 1)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 0)

    def test_spin_profile(self):
        self.spin_coater.spin_profile([(1000, 5000, 2), (2000, 5000, 2), (1000, 5000, 2)], wait=False)
        time.sleep(1)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 1000, 100)
        time.sleep(2)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 2000, 100)
        time.sleep(2)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 1000, 100)
        time.sleep(2)
        assert_float_equal(self.spin_coater.axis.current_velocity_rpm, 0, 5)
        assert_equal(self.spin_coater.spin_profile_running, False)
        time.sleep(5)

    def test_cover(self):
        self.spin_coater.open_cover()
        assert_equal(self.controller.output(self.spin_coater.cover.output_number), 1)
        time.sleep(0.5)
        self.spin_coater.close_cover()
        assert_equal(self.controller.output(self.spin_coater.cover.output_number), 0)

    def test_suction(self):
        self.spin_coater.suction_on()
        assert_equal(self.controller.output(self.spin_coater.suction.output_number), 1)
        time.sleep(0.5)
        self.spin_coater.suction_off()
        assert_equal(self.controller.output(self.spin_coater.suction.output_number), 0)