import unittest
from north_utils.test import assert_equal, assert_not_equal, assert_float_equal, assert_vector_equal, assert_list_items_equal, location_equal
from north_robots.components import Vector3, Quaternion, Location, LocationPlaceholder, Component, Grid, GridTray


class ComponentsTest(unittest.TestCase):
    def test_location_init(self):
        location = Location()
        assert_vector_equal(location.position, Vector3(0, 0, 0))
        assert_vector_equal(location.rotation.toEulerAngles(), Vector3(0, 0, 0))

        location = Location(1.0, 2.0, 3.0)
        assert_vector_equal(location.position, Vector3(1.0, 2.0, 3.0))
        assert_vector_equal(location.rotation.toEulerAngles(), Vector3(0, 0, 0))

        location = Location(1.0, 2.0, 3.0, 90, 0, 0)
        assert_vector_equal(location.position, Vector3(1.0, 2.0, 3.0))
        assert_vector_equal(location.rotation.toEulerAngles(), Vector3(90, 0, 0), 0.05)

        location = Location(1.0, 2.0, 3.0, 0, 0, 90)
        assert_vector_equal(location.position, Vector3(1.0, 2.0, 3.0))
        assert_vector_equal(location.rotation.toEulerAngles(), Vector3(0, 0, 90), 0.05)

        location = Location(local_position=Vector3(0, 1, 2))
        assert_vector_equal(location.position, Vector3(0.0, 1.0, 2.0))

        location = Location(local_rotation=Quaternion.fromEulerAngles(0, 90, 0))
        assert_vector_equal(location.rotation.toEulerAngles(), Vector3(0, 90, 0), 0.05)

    def test_location_parent(self):
        parent = Location()
        location = Location(parent=parent)
        assert_vector_equal(location.position, Vector3(0, 0, 0), 0.05)
        assert_vector_equal(location.rotation.toEulerAngles(), Vector3(0, 0, 0), 0.05)
        assert_equal(location.parent, parent)
        assert_not_equal(parent.parent, location.parent)
        assert_equal(parent.parent.parent, None)

        parent = Location(1, 1, 1)
        location = Location(parent=parent)
        assert_vector_equal(location.position, Vector3(1, 1, 1), 0.05)

    def test_location_transform(self):
        parent = Location(1, 1, 1)
        location = Location(parent=parent)
        assert_vector_equal(location.world_position_to_local(Vector3(1, 1, 1)), Vector3(0, 0, 0), 0.05)
        assert_vector_equal(location.world_position_to_local(Vector3(-1, 0, 10)), Vector3(-2, -1, 9), 0.05)

        # rotations are counter-clockwise
        parent = Location(rz=90)
        location = Location(local_position=Vector3(1, 0, 0), parent=parent)
        assert_vector_equal(location.position, Vector3(0, 1, 0), 0.05)

        parent = Location(rz=270)
        location = Location(local_position=Vector3(1, 0, 0), parent=parent)
        assert_vector_equal(location.position, Vector3(0, -1, 0), 0.05)

        parent = Location(rz=-90)
        location_a = Location(local_position=Vector3(1, 0, 0), ry=90, parent=parent)
        location_b = Location(local_position=Vector3(1, 0, 0), parent=location_a)
        assert_vector_equal(location_a.position, Vector3(0, -1, 0), 0.05)
        assert_vector_equal(location_b.position, Vector3(0, -1, -1), 0.05)
        # TODO: test local_position_to_world more thoroughly
        # assert_vector_equal(location_b.local_position_to_world(Vector3(0, 0, 0)), Vector3(0, -1, -1), 0.05)

    def test_component(self):
        component = Component('component')
        assert_vector_equal(component.location.position, Vector3(0, 0, 0))

        class CompTest(Component):
            placeholder = LocationPlaceholder()

        component = CompTest('comp_test')
        component.settings.data['placeholder'] = [1, 2, 3, 0, 0, 0]
        component.settings.save()

        component = CompTest('comp_test')
        assert_vector_equal(component.placeholder.position, Vector3(1, 2, 3), 0.05)
        assert_equal(component.placeholder.located, True)
        assert_not_equal(CompTest.placeholder, component.placeholder)

    def test_grid(self):
        grid = Grid('grid')
        assert_vector_equal(grid.locations.A1.position, Vector3(0, 40, 0), 0.05)
        assert_vector_equal(grid.locations.A2.position, Vector3(0, 30, 0), 0.05)
        assert_vector_equal(grid.locations.A3.position, Vector3(0, 20, 0), 0.05)
        assert_vector_equal(grid.locations.B1.position, Vector3(10, 40, 0), 0.05)
        assert_vector_equal(grid.locations.B2.position, Vector3(10, 30, 0), 0.05)
        assert_vector_equal(grid.locations.B3.position, Vector3(10, 20, 0), 0.05)

        grid = Grid('grid', location=Location(rz=90))
        assert_vector_equal(grid.locations.A1.position, Vector3(-40, 0, 0), 0.05)
        assert_vector_equal(grid.locations.A2.position, Vector3(-30, 0, 0), 0.05)
        assert_vector_equal(grid.locations.A3.position, Vector3(-20, 0, 0), 0.05)
        assert_vector_equal(grid.locations.B1.position, Vector3(-40, 10, 0), 0.05)
        assert_vector_equal(grid.locations.B2.position, Vector3(-30, 10, 0), 0.05)
        assert_vector_equal(grid.locations.B3.position, Vector3(-20, 10, 0), 0.05)

    def test_grid_tray_defaults(self):
        tray = GridTray(Location(0, 0, 0), 2, 3, (10, 10))
        indexes = tray.build_indexes()
        print(tray.indexes)
        print(indexes)
        assert_list_items_equal(tray.indexes, [['A1', 'B1', 'C1'], ['A2', 'B2', 'C2']])
        assert_list_items_equal(indexes, ['A2', 'B2', 'C2', 'A1', 'B1', 'C1'])

    def test_grid_tray_labels(self):
        args = (Location(0, 0, 0), 2, 3, (10, 10))

        tray = GridTray(*args, label_start=GridTray.TOP_LEFT, label_direction=GridTray.RIGHT)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['A1', 'A2', 'A3'], ['B1', 'B2', 'B3']])
        assert_list_items_equal(indexes, ['B1', 'B2', 'B3', 'A1', 'A2', 'A3'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.TOP_RIGHT, label_direction=GridTray.LEFT)
        print(tray.grid[0])
        print(tray.grid[1])
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['A3', 'A2', 'A1'], ['B3', 'B2', 'B1']])
        assert_list_items_equal(indexes, ['B3', 'B2', 'B1', 'A3', 'A2', 'A1'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.BOTTOM_LEFT, label_direction=GridTray.RIGHT)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['B1', 'B2', 'B3'], ['A1', 'A2', 'A3']])
        assert_list_items_equal(indexes, ['A1', 'A2', 'A3', 'B1', 'B2', 'B3'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.BOTTOM_RIGHT, label_direction=GridTray.LEFT)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['B3', 'B2', 'B1'], ['A3', 'A2', 'A1']])
        assert_list_items_equal(indexes, ['A3', 'A2', 'A1', 'B3', 'B2', 'B1'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.TOP_LEFT, label_direction=GridTray.DOWN)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['A1', 'B1', 'C1'], ['A2', 'B2', 'C2']])
        assert_list_items_equal(indexes, ['A2', 'B2', 'C2', 'A1', 'B1', 'C1'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.BOTTOM_LEFT, label_direction=GridTray.UP)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['A2', 'B2', 'C2'], ['A1', 'B1', 'C1']])
        assert_list_items_equal(indexes, ['A1', 'B1', 'C1', 'A2', 'B2', 'C2'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.TOP_RIGHT, label_direction=GridTray.DOWN)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['C1', 'B1', 'A1'], ['C2', 'B2', 'A2']])
        assert_list_items_equal(indexes, ['C2', 'B2', 'A2', 'C1', 'B1', 'A1'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

        tray = GridTray(*args, label_start=GridTray.BOTTOM_RIGHT, label_direction=GridTray.UP)
        indexes = tray.build_indexes()
        assert_list_items_equal(tray.indexes, [['C2', 'B2', 'A2'], ['C1', 'B1', 'A1']])
        assert_list_items_equal(indexes, ['C1', 'B1', 'A1', 'C2', 'B2', 'A2'])
        assert_list_items_equal(tray.grid, [
                                    [Location(0, 10, 0), Location(10, 10, 0), Location(20, 10, 0)],
                                    [Location(0, 0, 0), Location(10, 0, 0), Location(20, 0, 0)],
                                ], is_equal=location_equal)

    def test_grid_tray_indexes(self):
        args = (Location(0, 0, 0), 2, 3, (10, 10))
        tray = GridTray(*args, label_start=GridTray.TOP_LEFT, label_direction=GridTray.RIGHT)
        assert_list_items_equal(tray.indexes, [['A1', 'A2', 'A3'], ['B1', 'B2', 'B3']])

        indexes = tray.build_indexes(index_start=GridTray.TOP_LEFT, index_direction=GridTray.RIGHT)
        assert_list_items_equal(indexes, ['A1', 'A2', 'A3', 'B1', 'B2', 'B3'])

        indexes = tray.build_indexes(index_start=GridTray.BOTTOM_LEFT, index_direction=GridTray.RIGHT)
        assert_list_items_equal(indexes, ['B1', 'B2', 'B3', 'A1', 'A2', 'A3'])

        indexes = tray.build_indexes(index_start=GridTray.TOP_RIGHT, index_direction=GridTray.LEFT)
        assert_list_items_equal(indexes, ['A3', 'A2', 'A1', 'B3', 'B2', 'B1'])

        indexes = tray.build_indexes(index_start=GridTray.BOTTOM_RIGHT, index_direction=GridTray.LEFT)
        assert_list_items_equal(indexes, ['B3', 'B2', 'B1', 'A3', 'A2', 'A1'])

        indexes = tray.build_indexes(index_start=GridTray.TOP_LEFT, index_direction=GridTray.DOWN)
        assert_list_items_equal(indexes, ['A1', 'B1', 'A2', 'B2', 'A3', 'B3'])

        indexes = tray.build_indexes(index_start=GridTray.BOTTOM_LEFT, index_direction=GridTray.UP)
        assert_list_items_equal(indexes, ['B1', 'A1', 'B2', 'A2', 'B3', 'A3'])

        indexes = tray.build_indexes(index_start=GridTray.TOP_RIGHT, index_direction=GridTray.DOWN)
        assert_list_items_equal(indexes, ['A3', 'B3', 'A2', 'B2', 'A1', 'B1'])

        indexes = tray.build_indexes(index_start=GridTray.BOTTOM_RIGHT, index_direction=GridTray.UP)
        assert_list_items_equal(indexes, ['B3', 'A3', 'B2', 'A2', 'B1', 'A1'])