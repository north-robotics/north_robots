import unittest
import time
from north_utils.test import assert_equal, assert_not_equal, assert_float_equal, assert_vector_equal
from north_robots.n9 import N9Robot, Vector3


def assert_move_equal(move, x=None, y=None, z=None, gripper=None):
    assert x == move.get('x', None)
    assert y == move.get('y', None)
    assert z == move.get('z', None)
    assert gripper == move.get('gripper', None)


class MockC9Controller:
    class connection:
        connected = True

    def __init__(self):
        self.x = 0
        self.y = 0
        self.z = 0
        self.gripper = 0
        self.velocity = 0
        self.acceleration = 0
        self.bias = 0
        self.moves = []
        self.probes = []

    @property
    def probe(self):
        if len(self.probes) == 0:
            return None

        return self.probes[-1]['probe']

    @property
    def probe_offset(self):
        if len(self.probes) == 0:
            return None

        return self.probes[-1]['probe_offset']

    def cartesian_position(self):
        return self.x, self.y, self.z

    def speed(self, velocity=None, acceleration=None):
        if velocity is not None:
            self.velocity = velocity

        if acceleration is not None:
            self.acceleration = acceleration

        return velocity, acceleration

    def elbow_bias(self, bias):
        self.bias = bias

    def home(self, **kwargs):
        pass

    def move_arm(self, **kwargs):
        self.moves.append(kwargs)

        self.x = kwargs.get('x', self.x)
        self.y = kwargs.get('y', self.y)
        self.z = kwargs.get('z', self.z)
        self.gripper = kwargs.get('gripper', self.gripper)

    def use_probe(self, probe=True, probe_offset=41.5):
        self.probes.append({'probe': probe, 'probe_offset': probe_offset})


class N9Test(unittest.TestCase):
    def setUp(self):
        self.controller = MockC9Controller()
        self.n9 = N9Robot(self.controller)

    def test_move(self):
        self.n9.move(100, 100, 100)
        assert_equal(len(self.controller.moves), 1)
        assert_vector_equal(self.n9.location, Vector3(100, 100, 100))

    def test_move_z_xy(self):
        self.n9.move(100, 100, 100, order=self.n9.MOVE_Z_XY)
        assert_equal(len(self.controller.moves), 2)
        assert_move_equal(self.controller.moves[-2], z=100)
        assert_move_equal(self.controller.moves[-1], x=100, y=100)

    def test_move_xy_z(self):
        self.n9.move(100, 100, 100, order=self.n9.MOVE_XY_Z)
        assert_equal(len(self.controller.moves), 2)
        assert_move_equal(self.controller.moves[-2], x=100, y=100)
        assert_move_equal(self.controller.moves[-1], z=100)

    def test_move_xyz(self):
        self.n9.move(100, 100, 100, order=self.n9.MOVE_XYZ)
        assert_equal(len(self.controller.moves), 1)
        assert_move_equal(self.controller.moves[-1], x=100, y=100, z=100)

    def test_probe_reset_on_first_move(self):
        assert_equal(self.controller.probe, None)
        self.n9.move(100, 100, 100)
        assert_equal(self.controller.probe, False)

    def test_gripper_offset(self):
        self.n9.move(100, 100, 100, gripper_offset=[5, 10, 15])
        assert_vector_equal(self.n9.location, Vector3(95, 90, 85))

    def test_gripper_offset_with_rotation(self):
        self.n9.move(100, 100, 100, gripper=90, gripper_offset=[0, 10, 0])
        assert_vector_equal(self.n9.location, Vector3(90, 100, 100))

        self.n9.move(100, 100, 100, gripper=-90, gripper_offset=[0, 10, 0])
        assert_vector_equal(self.n9.location, Vector3(110, 100, 100))

        self.n9.move(100, 100, 100, gripper=180, gripper_offset=[0, 10, 0])
        assert_vector_equal(self.n9.location, Vector3(100, 110, 100))

        self.n9.move(100, 100, 100, gripper=45, gripper_offset=[0, 10, 0])
        assert_vector_equal(self.n9.location, Vector3(92, 92, 100), diff=1.0)

    def test_probe(self):
        self.n9.move(100, 100, 100, probe=True)
        # make sure the probe is toggled during the move
        assert_equal(self.controller.probes[-1]['probe'], False)
        assert_equal(self.controller.probes[-2]['probe'], True)
        assert_equal(self.controller.probes[-2]['probe_offset'], 41.5)

    def test_probe_offset(self):
        self.n9.move(100, 100, 100, probe=True, probe_offset=10)
        assert_equal(self.controller.probes[-2]['probe'], True)
        assert_equal(self.controller.probes[-2]['probe_offset'], 10)