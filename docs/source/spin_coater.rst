north\_robots.spin\_coater
==========================

.. py:currentmodule:: north_robots.spin_coater

----------
SpinCoater
----------

.. autoclass:: SpinCoater
  :members:
  :show-inheritance:

----------
Exceptions
----------

SpinCoaterError
---------------

.. autoclass:: SpinCoaterError
  :show-inheritance:

SpinCoaterProfileRunningError
-----------------------------

.. autoclass:: SpinCoaterProfileRunningError
  :show-inheritance:
