north_robots - High-level Control of North Robotics Robots
==========================================================

This package provides high-level classes for control of North Robotics robots like the N9 and spin coater.

Contents
--------

.. toctree::
  :maxdepth: 3

  n9
  spin_coater
  components
  managers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
