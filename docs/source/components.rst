========================
north\_robots.components
========================

.. py:currentmodule:: north_robots.components

The components module contains classes handling locations and *Trays*, which are groups of locations in various layouts.

--------
Location
--------

.. autoclass:: Location


----
Tray
----

.. autoclass:: Tray

--------
GridTray
--------

.. autoclass:: GridTray
  :show-inheritance:

----------
Exceptions
----------

ComponentError
--------------

.. autoclass:: ComponentError
  :show-inheritance:

ComponentChildNotFoundError
---------------------------

.. autoclass:: ComponentChildNotFoundError
  :show-inheritance:

LocationError
-------------

.. autoclass:: LocationError
  :show-inheritance:

LocationChildExistsError
------------------------

.. autoclass:: LocationChildExistsError
  :show-inheritance:

LocationChildNotFoundError
--------------------------

.. autoclass:: LocationChildNotFoundError
  :show-inheritance:

ComponentSettingsError
----------------------

.. autoclass:: ComponentSettingsError
  :show-inheritance:

ComponentSettingsJsonError
--------------------------

.. autoclass:: ComponentSettingsJsonError
  :show-inheritance: