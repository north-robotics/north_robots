===================
Installing north_c9
===================

Requirements
------------

The ``north_c9`` library requires Python version 3.6 or 3.7. Python 3.8 currently isn't supported.


Using requirements.txt
----------------------

To install the north_c9 package add this line to the end of the ``requirements.txt`` file inside your project directory:

  north_c9

Using pip
---------

To install the north_c9 package using pip run this on the command line:

  pip install north_c9

