======================
north\_robots.managers
======================

.. py:currentmodule:: north_robots.managers

-----------
TrayManager
-----------

.. autoclass:: TrayManager
  :members:

-----------------
NeedleTrayManager
-----------------

.. autoclass:: NeedleTrayManager
  :members:
  :show-inheritance:

----------
Exceptions
----------

ManagerError
------------

.. autoclass:: ManagerError
  :show-inheritance:

TrayManagerError
----------------

.. autoclass:: TrayManagerError
  :show-inheritance:

TrayManagerEmptyError
---------------------

.. autoclass:: TrayManagerEmptyError
  :show-inheritance:
