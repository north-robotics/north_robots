from typing import List
from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot, MovementOrder
from north_robots.components import Component, Grid, ComponentLocation, Location, Vector3


class PipetteTray(Component):
    def __init__(self, name: str, robot: N9Robot, *args, **kwargs):
        Component.__init__(self, name, *args, use_probe=True, **kwargs)
        self.robot = robot

        self.grid = Grid('pipette_grid', rows=3, columns=16, parent=self, use_probe=True)
        self.grid_safe_location = ComponentLocation('grid_safe_location', parent=self, use_probe=True)

        self.indexes: List[str] = []
        self.refresh_indexes()

    def refresh_indexes(self):
        self.indexes = self.grid.grid_indexes(reverse_rows=True)

    @property
    def pipette_location(self):
        return self.grid.locations[self.indexes[0]]

    def next_pipette(self):
        self.indexes.pop(0)

    def pickup_pipette(self):
        location = self.pipette_location
        safe_location = location.copy(z=self.grid_safe_location.location.z())
        self.robot.move_to_locations([safe_location, location, safe_location], gripper=0, order=N9Robot.MOVE_Z_XY, probe=True)
        self.next_pipette()


class PipetteRemover(Component):
    def __init__(self, name: str, robot: N9Robot, *args, **kwargs):
        Component.__init__(self, name, *args, use_probe=True, **kwargs)
        self.robot = robot

        self.remover_top_location = ComponentLocation('remover_top', parent=self, use_probe=True)
        self.remover_safe_location = ComponentLocation('remover_safe_location', parent=self, use_probe=True)
        self.remover_approach_location = ComponentLocation('remover_approach', parent=self, use_probe=True)

    def remove_pipette(self):
        safe_approach = self.remover_approach_location.location.copy(z=self.remover_safe_location.location.z())
        safe_remover = self.remover_top_location.location.copy(z=self.remover_safe_location.location.z())
        self.robot.move_to_locations([
            safe_approach,
            self.remover_approach_location,
            self.remover_top_location,
            safe_remover
        ], probe=True)


controller = C9Controller()
n9 = N9Robot(controller, velocity_counts=20_000, acceleration_counts=50_000, home=True)

remover = PipetteRemover('pipette_remover', n9)

Component.locate_all(controller)

tray = PipetteTray('pipette_tray', n9)

Component.locate_all(controller)

while True:
    tray.pickup_pipette()
    remover.remove_pipette()