import time
import signal
def signal_handler(sig, frame):
    print('Received signal {signal}'.format(signal=sig))

signal.signal(signal.SIGINT, signal_handler)
print('Press the stop button.')

try:
    time.sleep(60 * 60)
except Exception as err:
    print(err)