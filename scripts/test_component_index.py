from typing import List
from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot, MovementOrder
from north_robots.components import Component, Grid, LocationPlaceholder, Location, Vector3


class PipetteTray(Component):
    remover_top_location = LocationPlaceholder(use_probe=True)
    remover_approach_location = LocationPlaceholder(use_probe=True)
    remover_safe_location = LocationPlaceholder(use_probe=True)

    grid_safe_location = LocationPlaceholder(use_probe=True)

    def __init__(self, name: str, robot: N9Robot, *args, **kwargs):
        Component.__init__(self, name, *args, use_probe=True, **kwargs)
        self.robot = robot

        self.grid = Grid('pipette_grid', rows=3, columns=16, parent=self, use_probe=True)

        self.indexes: List[str] = []
        self.refresh_indexes()

    def refresh_indexes(self):
        self.indexes = self.grid.grid_indexes(reverse_rows=True)

    @property
    def pipette_location(self):
        return self.grid.locations[self.indexes[0]]

    def next_pipette(self):
        self.indexes.pop(0)

    def pickup_pipette(self):
        location = self.pipette_location
        safe_location = location.copy(z=self.grid_safe_location.z)
        self.robot.move_to_locations([safe_location, location, safe_location], gripper=0, order=N9Robot.MOVE_Z_XY, probe=True)
        self.next_pipette()

    def remove_pipette(self):
        safe_approach = self.remover_approach_location.copy(z=self.remover_safe_location.z)
        safe_remover = self.remover_top_location.copy(z=self.remover_safe_location.z)

        self.robot.move_to_locations([
            safe_approach,
            self.remover_approach_location,
            self.remover_top_location,
            safe_remover
        ], probe=True)


controller = C9Controller(verbose=True, connect=False)
n9 = N9Robot(controller, velocity_counts=20_000, acceleration_counts=50_000, home=True)
tray = PipetteTray('pipette_tray', n9)
Component.locate_all(controller)
while True:
    tray.pickup_pipette()
    tray.remove_pipette()