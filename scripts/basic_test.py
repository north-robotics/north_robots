import time
import logging
from ftdi_serial import Serial
from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot
from north_robots.spin_coater import SpinCoater

logging.basicConfig(level=logging.DEBUG)


serial = Serial()

# c9_n9 = C9Controller(serial, address=1)
c9_spin_coater = C9Controller(serial, address=2)

# n9 = N9Robot(c9_n9)
spin_coater = SpinCoater(c9_spin_coater)

# n9.home()
spin_coater.home()
while True:
    spin_coater.spin_profile([(1000, 1000, 10), (5000, 5000, 10), (1000, 1000, 10)], wait=False)
    spin_coater.wait_for_stop()

# time.sleep(2)
# while spin_coater.spinning:
    # n9.move(100, 150, 250)
    # n9.move(0, 250, 150)
print('done')
